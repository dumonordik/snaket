// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Food.h"
#include "Bonus.h"
#include "Math/UnrealMathUtility.h"
#include "K2Node_SpawnActor.h"
// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject <UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
		
		AddRandomFood();
		AddRandomBonus();
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	
}

void APlayerPawnBase::CreateSnakeActor()
{
	if(GetWorld())
	{
		SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
		
	}
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if(IsValid(SnakeActor))
	{
		if (value > 0)
		{
			SnakeActor->TempMovementDirection = EMovementDirection::UP;
		}
		else if (value < 0 )
		{
			SnakeActor->TempMovementDirection = EMovementDirection::DOWN;
		}

	}
	
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if(IsValid(SnakeActor))
	{
		if (value > 0 )
		{
			SnakeActor->TempMovementDirection  = EMovementDirection::RIGHT;
		}
		else if (value < 0 )
		{
			SnakeActor->TempMovementDirection = EMovementDirection::LEFT;
		}

	}
}




int32 APlayerPawnBase::GetScore()
{
	if(SnakeActor)
	{
		return SnakeActor->score;
	}
	return 0;
}
void APlayerPawnBase::AddRandomFood()
{
	FRotator StartPointRotation=FRotator(0,0,0);
	float SpawnX=FMath::FRandRange(MinX,MaxX);
	float SpawnY=FMath::FRandRange(MinY,MaxY);

	FVector StartPoint=FVector(SpawnX,SpawnY,SpawnZ);
	if(SnakeActor->ScoreFood<1)
	{
		if (IsValid(SnakeActor))
		{
		
			if(GetWorld())
			{
				
				GetWorld()-> SpawnActor<AFood>( FoodBP, StartPoint, StartPointRotation);
				SnakeActor->ScoreFood++;
			}
		}
	}
}
	void APlayerPawnBase::AddRandomBonus()
	{
	   
		FRotator StartPointRotation=FRotator(0,0,0);
	   	float SpawnX=FMath::FRandRange(MinX,MaxX);
	   	float SpawnY=FMath::FRandRange(MinY,MaxY);
	   	FVector StartPoint=FVector(SpawnX,SpawnY,SpawnZ);
	   	if((SnakeActor->ScoreBonus<1)&(SnakeActor->bon<1))
	   	{
	   		if (IsValid(SnakeActor))
	   		{
		
	   			if(GetWorld())
				
	   			GetWorld()-> SpawnActor<ABonus>( BonusBP, StartPoint, StartPointRotation);
	   			SnakeActor->ScoreBonus++;
	   			
	   		}
	   	}
	   
	}
	




// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "interactable.h"
#include "PlayerPawnBase.h"
#include "GameFramework/Actor.h"
#include "Food.generated.h"

class UStaticMeshComponent;
class AFood;
class APlayerPawnBase;
UCLASS()
class SNAKETEST_API AFood : public AActor, public Iinteractable
{
	GENERATED_BODY()
	
	public:	
	// Sets default values for this actor's properties
	AFood();
	
	UPROPERTY(EditDefaultsOnly, Category="Food")
		TSubclassOf<AFood> FoodBP;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* SnakeEatMesh;
	UPROPERTY(BlueprintReadOnly)
		APlayerPawnBase* Pawn;

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	class USphereComponent* MyRootComponent;

	
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "K2Node_SpawnActorFromClass.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SnakeEatMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	
	if(bIsHead)
	{
		auto Snake=Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy(true,true);
			Snake->score++;
			Snake->ScoreFood--;
			Snake->SetActorTickInterval(0.5);
			if(Snake->bon>0)
			{
				Snake->bon--;
			}
			
		}
	}
}



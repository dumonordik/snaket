// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "interactable.h"

#include "PlayerPawnBase.h"
#include "GameFramework/Actor.h"
#include "Bonus.generated.h"

class UStaticMeshComponent;
class ABonus;

class PlayerPawnBase;
UCLASS()
class SNAKETEST_API ABonus : public AActor, public Iinteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonus();

	UPROPERTY(EditDefaultsOnly, Category="Food")
		TSubclassOf<ABonus> BonusBP;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* SnakeBonusMesh;
	UPROPERTY(BlueprintReadWrite)
		APlayerPawnBase* Pawn;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool baIsHead) override;
	class USphereComponent* MyRootComponent;
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 13.f;
	LastMoveDirection = EMovementDirection::DOWN;
	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(0.5);
	AddSnakeElement(3);
	
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	
	Super::Tick(DeltaTime);
	Move();
}
 void  ASnakeBase::AddSnakeElement(int ElementsNum)
{
	
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(0, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SetActorHiddenInGame(true);
		NewSnakeElem->SnakeOwner=this;
		int32 ElemIndex=SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
		
	}
}
void ASnakeBase::Move()
{
	TrySwitchDirection(CurrentMovementDirection);

	FVector MovementDirection(ForceInitToZero);
	const auto Movement = ElementSize;

	switch (CurrentMovementDirection)
	{
	case EMovementDirection::UP:
		MovementDirection.X += Movement;
		break;
	case EMovementDirection::DOWN:
		MovementDirection.X -= Movement;
		break;
	case EMovementDirection::LEFT:
		MovementDirection.Y += Movement;
		break;
	case EMovementDirection::RIGHT:
		MovementDirection.Y -= Movement;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (auto i = SnakeElements.Num() - 1; i > 0; i--)
	{
		SnakeElements[i]->SetActorHiddenInGame(false);
		auto CurrentElement = SnakeElements[i];
		auto PreviousElement = SnakeElements[i - 1];
		auto PreviousPosition = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PreviousPosition);
	}

	SnakeElements[0]->SetActorHiddenInGame(false);
	SnakeElements[0]->AddActorWorldOffset(MovementDirection);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::TrySwitchDirection(EMovementDirection& Current)
{
	bool MovementValid;

	switch (Current)
	{
	case EMovementDirection::UP:
		MovementValid = TempMovementDirection != EMovementDirection::DOWN;
		break;
	case EMovementDirection::DOWN:
		MovementValid = TempMovementDirection != EMovementDirection::UP;
		break;
	case EMovementDirection::LEFT:
		MovementValid = TempMovementDirection != EMovementDirection::RIGHT;
		break;
	case EMovementDirection::RIGHT:
		MovementValid = TempMovementDirection != EMovementDirection::LEFT;
		break;
	default:
		MovementValid = false;
	}

	if (MovementValid)
	{
		Current = TempMovementDirection;
	}
}




void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst=ElemIndex==0;
		Iinteractable* InteractableInterface=Cast<Iinteractable>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}


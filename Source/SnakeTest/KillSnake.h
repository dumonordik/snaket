// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "interactable.h"
#include "GameFramework/Actor.h"
#include "KillSnake.generated.h"

UCLASS()
class SNAKETEST_API AKillSnake : public AActor, public Iinteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKillSnake();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};
UCLASS()
class SNAKETEST_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;
	UPROPERTY(EditDefaultsOnly)
		float ElementSize;
	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;
	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;
	UPROPERTY()
		EMovementDirection CurrentMovementDirection;

	UPROPERTY()
		EMovementDirection TempMovementDirection;

	UPROPERTY()
		EMovementDirection LastMoveDirection;
	int32 score=0;
	int32 ScoreFood=0;
	int32 ScoreBonus=0;
	int32 bon=0;
	UPROPERTY()
		EMovementDirection lastmove;
 protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddSnakeElement(int ElementsNum=1);
	void Move();
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
private:
	void TrySwitchDirection(EMovementDirection& Current);

	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"
#include "K2Node_SpawnActorFromClass.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"
// Sets default values
ABonus::ABonus()
{
	
	PrimaryActorTick.bCanEverTick = true;
	SnakeBonusMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	
	
}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
		
	
}
void ABonus::Interact(AActor* Interactor, bool baIsHead)
{
	
	if(baIsHead)
	{
		auto Snake=Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Destroy(true,true);
			Snake->SetActorTickInterval(0.3);
			Snake->ScoreBonus--;
			Snake->bon=2;
		}
	}
}




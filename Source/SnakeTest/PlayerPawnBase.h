// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Food.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class ABonus;

UCLASS()
class SNAKETEST_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();
	
	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;
	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodBP;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonus> BonusBP;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	
	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
	UFUNCTION(BlueprintCallable, Category="SnakePawn")
		void CreateSnakeActor();
	UFUNCTION(BlueprintCallable, Category="BonusSPawn")
	void AddRandomFood();
	UFUNCTION(BlueprintCallable, Category="BonusSpawn")
	void AddRandomBonus();
	float MinY=-510.f; float MaxY=510.f;
	float MinX=-440.f; float MaxX=500.f;
	float SpawnZ=30.f;
	UFUNCTION(BlueprintCallable, Category="SnakePawn")
		int32 GetScore();
		
	
};
